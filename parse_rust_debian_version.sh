#!/bin/sh

sed '/^FROM/!d' debian.Dockerfile | tail -n 1 | cut -d':' -f2
